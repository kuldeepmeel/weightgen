# README #
WeightGen is hashing-based approximate weighted sampling for weighted CNF formulas. 
 The code is based on hashing-based framework developed over the years. See overview paper [here](http://www.comp.nus.edu.sg/~meel/Papers/BNP16.pdf) 

For more details on hashing-based approach, please visit: https://meelgroup.github.io

Installation and usage instructions can be found in the "INSTALL" file 


### Licensing ###
Please see the file `LICENSE-MIT`.



### Contact ###
* Kuldeep Meel (meel@comp.nus.edu.sg)
* Daniel Fremont (dfremont@rice.edu)
